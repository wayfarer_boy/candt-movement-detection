/* global window */

const prefixVibrate = [0, 110];
const vStep = 20;

const tones = {
  'ready': { timing: 0.45, freq: 440, vibrate: [...prefixVibrate, vStep * 2] },
  'bar': { timing: 0.15, freq: 1760, vibrate: [...prefixVibrate, vStep * 6] },
  'beat': { timing: 0.15, freq: 880, vibrate: [...prefixVibrate, vStep] },
  'barReady': { timing: 0.15, freq: 880, vibrate: [...prefixVibrate, vStep * 6] },
  'beatReady': { timing: 0.15, freq: 440, vibrate: [...prefixVibrate, vStep] },
  'go': { timing: 0.45, freq: 880, vibrate: [...prefixVibrate, vStep * 6] },
  'done': { timing: 0.75, freq: 1760, vibrate: [...prefixVibrate, vStep * 3, vStep * 2, vStep * 3] },
};

const volArr = new Float32Array(2);
volArr[0] = 1;
volArr[1] = 0;
const context = new (window.AudioContext || window.webkitAudioContext)();

class Sample {
  constructor(key) {
    const { timing, freq, vibrate } = tones[key];
    this.timing = timing;
    this.freq = freq;
    this.vibrate = vibrate;
    this.key = key;
  }

  play(times) {
    this.gain = context.createGain();
    this.gain.connect(context.destination);
    this.oscillator = context.createOscillator();
    this.oscillator.addEventListener('ended', this.ended.bind(this));
    this.oscillator.type = 'sine';
    this.oscillator.connect(this.gain);
    const now = context.currentTime;
    this.gain.gain.setValueAtTime(0, now);
    times.forEach((time) => {
      this.gain.gain.setValueAtTime(0, now + (time / 1000));
      this.gain.gain.setValueCurveAtTime(volArr, parseFloat(now + (time / 1000)), this.timing);
      // if (vibrate) window.navigator.vibrate(tones[key].vibrate);
    });
    this.oscillator.frequency.value = this.freq;
    this.oscillator.start(now);
    this.oscillator.stop(now + (times.pop() / 1000) + this.timing);
  }

  stop() {
    if (this.oscillator) {
      try {
        const now = context.currentTime;
        this.oscillator.stop(now + 0.15);
      } catch (err) {
        console.error(err);
      }
    }
  }

  ended() {
    this.oscillator.disconnect(this.gain);
    this.oscillator = null;
    this.gain.disconnect(context.destination);
    this.gain = null;
  }

}

const samples = {};
Object.keys(tones).forEach(key => {
  samples[key] = context ? new Sample(key) : {};
});

export const playTone = ({ tone, times }) => samples[tone] && samples[tone].play(times);
export const stopAll = () => Object.keys(samples).forEach(key => samples[key].stop());
