export const defaultValues = {
  name: '',
  duration: 5000,
  media: '',
  bpm: 120,
  bars: 4,
  beats: 4,
  isRhythmic: false,
  data: '',
};

export const initialState = [
  /*
  {
    name: 'Lookout',
    key: 1,
    duration: 4000,
    media: [],
    bpm: 120,
    bars: 4,
    beats: 4,
    isRhythmic: false,
    data: '/lookout.png',
  }, {
    name: 'Punch',
    key: 2,
    duration: 5000,
    media: [],
    bpm: 120,
    bars: 4,
    beats: 4,
    isRhythmic: false,
    data: '/punch.png',
  }, {
    name: 'Waving',
    key: 3,
    duration: 3000,
    media: [],
    bpm: 120,
    bars: 4,
    beats: 4,
    isRhythmic: false,
    data: '/waving.png',
  },
  */
];
