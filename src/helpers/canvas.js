/* global document, Image */
import { imgWidth } from './data.js';

export default class Canvas {
  constructor(image) {
    const canvas = document.createElement('canvas');
    canvas.width = imgWidth;
    canvas.height = 0;
    const context = canvas.getContext('2d');
    if (image) {
      canvas.height = image.naturalHeight;
      context.drawImage(image, 0, 0);
    }
    this.canvas = canvas;
    this.context = context;
  }

  resample(newHeight) {
    let imageData;
    if (this.canvas.height) {
      imageData = this.context.getImageData(0, 0, imgWidth, this.canvas.height);
    }
    this.canvas.height = newHeight;
    if (imageData) this.context.putImageData(imageData, 0, 0);
  }

  addExample(image, sourceDims = [0, 0, imgWidth, imgWidth]) {
    const h = this.canvas.height;
    this.resample(h + imgWidth);
    this.context.drawImage(image, ...sourceDims, 0, h, imgWidth, imgWidth);
  }

  removeExample(index) {
    const remainingHeight = this.canvas.height - ((index + 1) * imgWidth);
    const src = [0, (index + 1) * imgWidth, imgWidth, remainingHeight];
    const dest = [0, index * imgWidth, imgWidth, remainingHeight];
    this.context.drawImage(this.canvas, ...src, ...dest);
    this.resample(this.canvas.height - imgWidth);
  }

  getSrc() {
    const url = this.canvas.toDataURL();
    if (url === 'data:,') return '';
    return url;
  }

  getNumSamples() {
    return this.canvas.height ? parseInt(this.canvas.height / imgWidth, 10) : 0;
  }

  getThumbs(asImages) {
    const canvas = document.createElement('canvas');
    canvas.width = imgWidth;
    canvas.height = imgWidth;
    const ctx = canvas.getContext('2d');
    const arr = Array
      .from(Array(this.getNumSamples()))
      .map((_, i) => {
        ctx.drawImage(this.canvas, 0, i * imgWidth, imgWidth, imgWidth, 0, 0, imgWidth, imgWidth);
        if (asImages) {
          return new Promise((res) => {
            const img = new Image(imgWidth, imgWidth);
            img.onload = () => res(img);
            img.src = canvas.toDataURL();
          });
        }
        return { backgroundImage: `url(${canvas.toDataURL()})` };
      });
    return arr;
  }

}
