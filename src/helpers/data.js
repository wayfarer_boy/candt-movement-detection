export const imgWidth = 224;
export const step = Math.max(40, Math.floor(3000 / imgWidth));
export const imgSize = [imgWidth, imgWidth, 3];
const black = [0, 0, 0];
const columns = Array.from(Array(imgWidth)).map(() => [...black]);

export const getMinMaxMotion = data => ({
  min: data.reduce((a, b) => black.map((_, i) => Math.min(a[i], b[i + 3])), [...black]),
  max: data.reduce((a, b) => black.map((_, i) => Math.max(a[i], b[i + 3])), [...black]),
});

export const drawColumn = ({ context, data, offset, minMaxMotion, width = 1 }) => {
  const colors = columns.map(arr => [...arr]);
  black.forEach((_, i) => {
    const motion = (data[i + 3] - minMaxMotion.min[i]) / (minMaxMotion.max[i] - minMaxMotion.min[i]);
    const height = (imgWidth * 0.5) * motion;
    let y = data[i] * imgWidth;
    let startY = y - height * 0.5;
    let endY = y + height * 0.5;
    columns.forEach((__, index) => {
      if (index >= startY && index <= endY) colors[index][i] = Math.round(motion * 255);
    });
    if (startY < 0 || endY > imgWidth) {
      if (startY < 0) {
        startY = y + imgWidth - height * 0.5;
        endY = imgWidth;
      } else if (endY > imgWidth) {
        endY = y - imgWidth + height * 0.5;
        startY = 0;
      }
      columns.forEach((__, index) => {
        if (index >= startY && index <= endY) colors[index][i] = Math.round(motion * 255);
      });
    }
  });
  colors.forEach((color, index) => {
    context.fillStyle = `rgb(${color.join(',')})`;
    context.fillRect(offset[0], index + offset[1], width, 1);
  });
};
