/*
 * action types
 */

export const ADD_MOVEMENT = 'ADD_MOVEMENT';
export const REMOVE_MOVEMENT = 'REMOVE_MOVEMENT';
export const UPDATE_MOVEMENT = 'UPDATE_MOVEMENT';
export const UPDATE_MEDIA = 'UPDATE_MEDIA';

/*
 * action creators
 */

export function addMovement(movement) {
  return { type: ADD_MOVEMENT, movement };
}

export function removeMovement(movement) {
  return { type: REMOVE_MOVEMENT, movement };
}

export function updateMovement(movement) {
  return { type: UPDATE_MOVEMENT, movement };
}

export function updateMedia(movement, media) {
  return { type: UPDATE_MEDIA, movement, media };
}
