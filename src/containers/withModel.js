/* global window, ml5 */
import { lifecycle, withState, withHandlers, setDisplayName, withProps, compose } from 'recompact';

import withTimeout from './withTimeout.js';

let features;
let classifier;

const withModel = component => compose(

  withTimeout,
  withState('epochs', 'setEpochs', 10),
  withState('loss', 'setLoss', []),
  withState('accuracy', 'setAccuracy'),
  withState('loaded', 'setLoaded', 0),
  withState('isTraining', 'setIsTraining', false),
  withState('mobilenetReady', 'setMobilenetReady', false),
  withState('trained', 'setTrained', false),

  withProps(({ movements, mobilenetReady }) => {
    const trainable = movements.length > 1 &&
      !movements.some(({ thumbs }) => thumbs.length === 0);
    const numSamples = movements.reduce((a, b) => b.thumbs && b.thumbs.length + a, 0);
    return {
      maxDuration: Math.max(...movements.map(({ totalDuration }) => totalDuration)),
      canTrain: mobilenetReady && trainable,
      trainable,
      numSamples,
    };
  }),

  withHandlers({

    onLoad: () => (repeat, done) => {
      if (window.ml5) {
        done();
      } else {
        repeat(repeat, done);
      }
    },

    onInit: () => (done) => {
      const opts = {
        learningRate: 0.5,
      };
      features = ml5.featureExtractor('MobileNet', opts, () => {
        classifier = features.classification();
        if (done) done();
      });
    },

    onAdd: () => (img, label) => {
      console.log('Added image', img);
      classifier.addImage(img, label).catch(console.error);
    },

    onAddInit: ({ setTimeout, movements, setIsTraining, setLoaded }) => () => {
      const promises = [];
      let loaded = 0;
      setLoaded(0);
      features.numClasses = movements.length;
      setTimeout(() => {
        movements.forEach(({ canvas, name }) => {
          promises.push(...canvas.getThumbs(true)
            .map((promise, index) => promise
              .then((img) => {
                if (img) {
                  return new Promise((res) => {
                    window.requestAnimationFrame(() => {
                      console.log('Adding sample', name, index);
                      classifier.addImage(img, name)
                        .catch(console.error)
                        .then(() => {
                          loaded += 1;
                          setLoaded(loaded);
                          res();
                        });
                    });
                  });
                }
                return Promise.reject('Invalid image');
              }),
            ),
          );
        });
        Promise.all(promises).then(() => setIsTraining(false));
      }, 500);
    },

    onAddSample: () => (sampling, img) => {
      classifier.addImage(img, sampling.name).catch((err) => {
        console.error(err);
      });
    },

    onTrain: ({ setTrained, setTimeout, setLoss, setIsTraining }) => () => {
      setIsTraining(true);
      setLoss([]);
      setTimeout(() => {
        const lossArr = [];
        classifier.train((loss) => {
          lossArr.push(Number(loss));
          console.log(loss);
          setLoss(lossArr);
        }).then(() => {
          setTrained(true);
        }).catch((err) => {
          console.error(err);
        }).then(() => {
          setIsTraining(false);
        });
      }, 500);
    },

    onPredict: () => (img) => {
      if (img) return classifier.classify(img);
      return Promise.resolve();
    },
  }),

  lifecycle({
    componentDidMount() {
      this.props.onLoad(this.props.onLoad, () => {
        this.props.onInit(() => {
          this.props.setMobilenetReady(true);
          if (this.props.trainable) this.props.onAddInit();
        });
      });
    },
  }),

  setDisplayName('MovementModel'),

)(component);

export default withModel;
