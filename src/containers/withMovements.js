/* global Image */
import { compose, withState, withHandlers, lifecycle, withProps, setDisplayName } from 'recompact';
import { connect } from 'react-redux';

import { updateMedia, addMovement, removeMovement, updateMovement } from '../actions.js';
import Canvas from '../helpers/canvas.js';
import { defaultValues } from '../helpers/movements.js';

const mapStateToProps = state => ({
  movements: state.movements,
});

const mapDispatchToProps = dispatch => ({
  addMovement: movement => dispatch(addMovement(movement)),
  removeMovement: movement => dispatch(removeMovement(movement)),
  updateMovement: movement => dispatch(updateMovement(movement)),
  updateMedia: (movement, media) => dispatch(updateMedia(movement, media)),
});

const withMovements = component => compose(
  connect(mapStateToProps, mapDispatchToProps),
  withState('styles', 'setStyles', {}),
  withState('canvases', 'setCanvases', {}),

  withProps(({ movements, styles, canvases }) => {
    const newMovements = movements.map((item) => {
      const text = [];
      if (styles[item.key]) {
        text.push(styles[item.key].length === 1 ? '1 example' : `${styles[item.key].length} examples`);
      } else {
        text.push('0 examples');
      }
      if (item.isRhythmic) {
        text.push(`${item.beats}-beat rhythm`);
        text.push(`${(item.beats * item.bars * (60 / Number(item.bpm))).toFixed(1)}s`);
      } else {
        text.push(`${parseInt(item.duration / 1000, 10)}s`);
      }
      return {
        ...item,
        thumbs: styles[item.key] || [],
        secondary: text.join(', '),
        canvas: canvases[item.key],
        totalDuration: item.isRhythmic ?
          item.beats * item.bars * (60 / Number(item.bpm)) * 1000 :
          item.duration,
      };
    });
    const neutral = newMovements.find(({ name }) => name === 'Neutral');
    return { movements: newMovements, neutral };
  }),

  withHandlers({

    onInitImages: ({ movements, setStyles, setCanvases }) => () => {
      const styles = {};
      const canvases = {};
      const addStyle = (style, key) => {
        styles[key] = style;
        if (Object.values(styles).length === movements.length) {
          setStyles(styles);
          setCanvases(canvases);
        }
      };
      if (movements.length) {
        movements.forEach(({ media, key }) => {
          if (media) {
            const img = new Image();
            img.onload = () => {
              canvases[key] = new Canvas(img);
              addStyle(canvases[key].getThumbs(), key);
            };
            img.src = media;
          } else {
            canvases[key] = new Canvas();
            addStyle([], key);
          }
        });
      } else {
        setStyles(styles);
        setCanvases(canvases);
      }
    },

    onUpdate: ({ updateMedia, styles, setStyles }) => (item) => {
      if (item) {
        const media = item.canvas.getSrc();
        const newStyles = { ...styles };
        newStyles[item.key] = item.canvas.getThumbs();
        setStyles(newStyles);
        updateMedia(item, media);
      }
    },

    onRemove: ({
      setStyles, setCanvases, removeMovement, canvases, styles,
    }) => (item) => () => {
      if (item) {
        setStyles({ ...styles, [item.key]: undefined });
        setCanvases({ ...canvases, [item.key]: undefined });
        removeMovement(item);
      }
    },

    onSave: ({
      setStyles, setCanvases, styles, canvases, movements, addMovement, updateMovement,
    }) => (document) => {
      const index = movements.findIndex(m => m.key === document.key);
      if (document.key && index > -1) {
        updateMovement(document);
      } else {
        const key = Date.now();
        const newMovement = { ...document, key };
        addMovement(newMovement);
        setStyles({ ...styles, [key]: [] });
        setCanvases({ ...canvases, [key]: new Canvas() });
      }
    },

  }),

  withHandlers({
    createNeutral: ({ onSave }) => () => {
      onSave({ ...defaultValues, name: 'Neutral' });
    },
  }),

  lifecycle({
    componentDidMount() {
      this.props.onInitImages();
    },
  }),

  setDisplayName('withMovements'),
)(component);

export default withMovements;
