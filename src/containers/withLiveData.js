/* global window */
import { withProps, setDisplayName, compose, withState, withHandlers, lifecycle } from 'recompact';

import withGyro from './withGyro.js';
import { getMinMaxMotion, imgWidth, drawColumn } from '../helpers/data.js';

let time = 0;

const withLiveData = component => compose(
  withGyro,
  withProps(({ movements }) => {
    const maxDuration = movements.reduce((a, b) => Math.max(a, b.totalDuration), 0);
    const step = Math.max(40, Math.floor(maxDuration / imgWidth));
    const colWidth = Math.max(1, Math.ceil(imgWidth / (maxDuration / step)));
    return { step, colWidth };
  }),
  withState('canvas', 'setCanvas'),
  withState('context', 'setContext'),
  withState('data', 'setData', []),
  withState('currentImage', 'setCurrentImage', ''),

  withHandlers({

    onTick: ({
      setData, data, context, getGyroData, disabled, step, colWidth,
    }) => done => newTime => {
      if (newTime - time >= step) {
        const scale = (newTime - time) / step;
        time += step * Math.floor(scale);
        if (!disabled) {
          if (context) {
            const width = Math.max(colWidth, Math.ceil(colWidth * scale));
            const newCol = getGyroData();
            const newData = [...data, newCol];
            if (newData.length > imgWidth) newData.shift();
            setData(newData);
            const minMaxMotion = getMinMaxMotion(newData);
            context.drawImage(context.canvas, width, 0, imgWidth - width, imgWidth, 0, 0, imgWidth - width, imgWidth);
            drawColumn({ minMaxMotion, data: newCol, context, offset: [imgWidth - width, 0], width: width });
          }
        }
      }
      if (done) window.requestAnimationFrame(done(done));
    },

    onSetCanvas: ({
      setCanvas, setContext, canvas: _canvas, context: _context, setCurrentImage,
    }) => el => {
      if (el) {
        let canvas = _canvas;
        if (!canvas) {
          canvas = el;
          setCanvas(canvas);
        }
        let context = _context;
        if (!context) {
          context = canvas.getContext('2d');
          setContext(context);
        }
        canvas.height = imgWidth;
        canvas.width = imgWidth;
        context.fillStyle = '#000';
        context.fillRect(0, 0, imgWidth, imgWidth);
        setCurrentImage(canvas.toDataURL());
      }
    },

    getCurrent: ({ currentImageEl, canvas, setCurrentImage }) => () => {
      setCurrentImage(canvas.toDataURL());
      return currentImageEl;
    },
  }),

  withHandlers({

    onInit: ({ onTick, step }) => () => {
      onTick(onTick)(step);
    },

  }),

  lifecycle({
    componentDidMount() {
      this.props.onInitGyro()
        .then((inited) => {
          if (inited) this.props.onInit();
        });
    },
    componentWillUnmount() {
      this.props.onDestroy();
    },
  }),
  setDisplayName('withLiveData'),
)(component);

export default withLiveData;
