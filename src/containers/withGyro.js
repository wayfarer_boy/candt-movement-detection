import { withHandlers, compose, withState, setDisplayName } from 'recompact';
import fulltilt from '../helpers/fulltilt.js';

const withGyro = component => compose(
  withState('orientation', 'setOrientation'),
  withState('motion', 'setMotion'),
  withState('gyroDisabled', 'setDisabled', false),

  withHandlers({

    onInitGyro: ({ setOrientation, setMotion, setDisabled }) => () => Promise.all([
      fulltilt.getDeviceOrientation({ type: 'game' }).then(o => setOrientation(o)),
      fulltilt.getDeviceMotion({ type: 'game' }).then(m => setMotion(m)),
    ]).catch(() => {
      setDisabled(true);
      return false;
    }),

    getGyroData: ({ orientation, motion }) => () => {
      const { alpha, beta, gamma } = orientation.getScreenAdjustedEuler();
      const { x, y, z } = motion.getScreenAdjustedAcceleration();
      return [
        ((alpha + 180) % 180) / 180,
        ((beta + 180) % 180) / 180,
        ((gamma + 180) % 180) / 180,
        x || 0,
        y || 0,
        z || 0,
      ];
    },
  }),

  setDisplayName('withGyro'),

)(component);

export default withGyro;
