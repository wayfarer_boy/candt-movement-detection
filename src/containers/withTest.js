import { compose, withHandlers, withState } from 'recompact';

export default component => compose(
  withState('prediction', 'setPrediction'),
  withState('isTesting', 'setIsTesting', false),

  withHandlers({
    onStart: ({ onPredict, getCurrent, maxDuration, isTesting }) => done => {
      onPredict(getCurrent(maxDuration)).then((result) => {
        console.log(result);
        if (isTesting) done(done);
      });
    },
  }),

  withHandlers({
    onTest: ({ onStart, isTesting, setIsTesting }) => () => {
      if (!isTesting) {
        setIsTesting(true);
        onStart(onStart);
      } else {
        setIsTesting(false);
      }
    },
  }),

)(component);
