/* global global */
/* eslint-disable no-empty */
import { compose, withHandlers, withState, lifecycle } from 'recompact';

const window = global.window || {
  setTimeout: func => func(),
  setInterval: func => func(),
  clearTimeout: () => {},
  clearInterval: () => {},
};

const withTimeout = component => compose(
  withState('timeouts', 'setTimeouts', []),
  withState('intervals', 'setIntervals', []),

  withHandlers({

    setTimeout: ({ timeouts, setTimeouts }) => (func, delay) => {
      const timeout = window.setTimeout(func, delay);
      setTimeouts([...timeouts, timeout]);
      return timeout;
    },

    setInterval: ({ intervals, setIntervals }) => (func, delay) => {
      const interval = window.setInterval(func, delay);
      setIntervals([...intervals, interval]);
      return interval;
    },

    clearTimeout: ({ timeouts, setTimeouts }) => (timeout) => {
      window.clearTimeout(timeout);
      setTimeouts(timeouts.filter(i => timeout !== i));
    },

    clearInterval: ({ intervals, setIntervals }) => (interval) => {
      window.clearInterval(interval);
      setIntervals(intervals.filter(i => interval !== i));
    },

  }),

  lifecycle({
    componentWillUnmount() {
      this.props.timeouts.forEach(t => window.clearTimeout(t));
      this.props.intervals.forEach(i => window.clearInterval(i));
    },
  }),
)(component);

export default withTimeout;
