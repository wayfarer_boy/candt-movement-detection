import { combineReducers } from 'redux';
import { UPDATE_MOVEMENT, ADD_MOVEMENT, REMOVE_MOVEMENT, UPDATE_MEDIA } from './actions.js';
import { initialState } from './helpers/movements.js';

const sortByName = (a, b) => a.name < b.name ? -1 : 1;

function movements(state = initialState, action) {
  switch (action.type) {
    case ADD_MOVEMENT:
      return [...state, action.movement].sort(sortByName);
    case REMOVE_MOVEMENT:
      return state.filter(m => m.key !== action.movement.key);
    case UPDATE_MOVEMENT:
      return [...state.filter(m => m.key !== action.movement.key), action.movement].sort(sortByName);
    case UPDATE_MEDIA:
      return [
        ...state.filter(m => m.key !== action.movement.key),
        { ...action.movement, media: action.media },
      ].sort(sortByName);
    default:
      return state;
  }
}

export default combineReducers({ movements });
