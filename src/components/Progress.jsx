/* global window */
import React from 'react';
import { number } from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import { compose, withState, withHandlers, lifecycle, setDisplayName } from 'recompact';
import withTimeout from '../containers/withTimeout.js';

let start = 0;
const Progress = ({ progress }) => (
  <CircularProgress variant="determinate" value={progress} size={96} thickness={10} />
);

Progress.propTypes = {
  progress: number,
};

export default compose(
  withTimeout,
  withState('progress', 'setProgress', 0),
  withHandlers({
    onTick: ({ duration, progress, setProgress, onStop }) => done => (time) => {
      if (progress >= 100) {
        start = 0;
        onStop();
      } else {
        if (!start) {
          start = time;
        } else {
          setProgress(Math.min(duration, time - start) / duration * 100);
        }
        window.requestAnimationFrame(done(done));
      }
    },
  }),
  withHandlers({
    onStart: ({ onTick }) => () => {
      window.requestAnimationFrame(onTick(onTick));
    },
  }),
  lifecycle({
    componentDidMount() {
      this.props.onStart();
    },
  }),
  setDisplayName('RecordingProgress'),
)(Progress);
