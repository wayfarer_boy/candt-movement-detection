import React from 'react';
import { bool, func, object, number } from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import { lifecycle, withProps, compose, withHandlers, withState } from 'recompact';
import Typography from '@material-ui/core/Typography';
import Zoom from '@material-ui/core/Zoom';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';

import { playTone, stopAll } from '../helpers/sfx.js';
import { imgWidth } from '../helpers/data.js';
import withTimeout from '../containers/withTimeout.js';
import Progress from './Progress.jsx';

const styles = theme => ({
  dialog: {
    height: `calc(100% - ${imgWidth}px)`,
  },
  wrapper: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    boxSizing: 'border-box',
    paddingTop: 64,
    paddingBottom: 64,
  },
  center: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '100%',
    padding: theme.spacing.unit * 4,
    boxSizing: 'border-box',
    textAlign: 'center',
  },
  countdown: { pointerEvents: 'none' },
  count: { paddingLeft: theme.spacing.unit * 2 },
});

const Countdown = ({
  recording, onStopRecording, countdown, classes, document, totalDuration,
}) => (
  <Dialog hideBackdrop fullScreen open classes={{ root: classes.dialog }}>
    <DialogTitle>{document.name}</DialogTitle>
    <DialogContent>
      <Grid direction="column" container className={classes.wrapper} alignItems="center" justify="center">
        <Grid item xs style={{ position: 'relative', width: '100%' }}>

          { Array.from(Array(4)).map((_, i) => <div className={`${classes.center} ${classes.countdown}`} key={`countdown-${i}`}>
            <Zoom in={countdown === i + 1}>
              <Typography variant="display4">{i + 1}</Typography>
            </Zoom>
          </div>) }

          <div className={`${classes.center} ${classes.progress}`}>
            <Zoom in={recording}>
              <div>
                { recording && <Progress duration={totalDuration} onStop={onStopRecording} /> }
              </div>
            </Zoom>
          </div>

        </Grid>
      </Grid>
    </DialogContent>
  </Dialog>
);

Countdown.propTypes = {
  classes: object,
  countdown: number,
  document: object,
  totalDuration: number,
  onStopRecording: func,
  recording: bool,
};

export default compose(
  withTimeout,
  withStyles(styles),

  withState('duration', 'setDuration', 0),
  withState('startTime', 'setStartTime', 0),
  withState('countdown', 'setCountdown', 0),
  withState('recording', 'setRecording', false),
  withState('samples', 'setSamples', ({ document }) => document.thumbs.length),

  withProps(({ document }) => {
    const totalDuration = document.isRhythmic ?
      document.beats * document.bars * (60 / Number(document.bpm)) * 1000 :
      document.duration;
    const beat = document.isRhythmic ? 60 / document.bpm * 1000 : 1000;
    return {
      totalDuration,
      beat,
    };
  }),

  withHandlers({

    onReset: ({ setDuration, setStartTime, setCountdown }) => () => {
      setDuration(0);
      setStartTime(0);
      setCountdown(-1);
    },

  }),

  withHandlers({

    onCountdown: ({ setCountdown, countdown, document, setRecording }) => done => () => {
      countdown += document.isRhythmic ? 1 : -1;
      setCountdown(countdown);
      if (countdown === (document.isRhythmic ? 5 : 0)) {
        setRecording(true);
        done();
      }
    },

  }),

  withHandlers({

    onStart: ({
      clearInterval, setCountdown, setInterval, document, beat, onCountdown, onSave,
    }) => () => {
      let countdown = document.isRhythmic ? 1 : 3;
      setCountdown(countdown);
      if (document.isRhythmic) {
        playTone({ tone: 'barReady', times: [0] });
        playTone({ tone: 'beatReady', times: Array.from(Array(document.beats - 1)).map((_, i) => beat * (i + 1)) });
        playTone({
          tone: 'bar',
          times: Array
            .from(Array(document.bars))
            .map((_, i) => (i * (beat * document.beats)) + (document.beats * beat)),
        });
        playTone({
          tone: 'beat',
          times: [].concat(...Array
            .from(Array(document.bars))
            .map((_, i) => {
              const offset = (i * (beat * document.beats)) + (document.beats * beat);
              return Array.from(Array(document.beats - 1)).map((__, idx) => {
                return offset + (beat * (idx + 1));
              });
            }),
          ),
        });
        playTone({ tone: 'done', times: [beat * document.beats * (document.bars + 1)] });
      } else {
        playTone({ tone: 'ready', times: [0, 1000, 2000] });
        playTone({ tone: 'go', times: [3000] });
        playTone({ tone: 'done', times: [3000 + document.duration] });
      }
      let interval;
      interval = setInterval(onCountdown(() => {
        onSave(true);
        clearInterval(interval);
      }), beat);
    },

  }),

  withHandlers({
    onStopRecording: ({ setTimeout, onSave, onStart, setRecording, setSamples, samples }) => () => {
      setRecording(false);
      onSave();
      setSamples(samples + 1);
      setTimeout(() => onStart(), 1000);
    },
  }),

  lifecycle({

    componentDidMount() {
      this.props.onStart();
    },

    componentWillUnmount() {
      stopAll();
    },

  }),

)(Countdown);
