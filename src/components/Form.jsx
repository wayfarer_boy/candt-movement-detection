import React from 'react';
import { bool, func, object } from 'prop-types';
import { withHandlers, setDisplayName, compose } from 'recompact';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Grid from '@material-ui/core/Grid';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

import { defaultValues } from '../helpers/movements.js';
import Bpm from './Bpm.jsx';

const styles = theme => ({
  numberField: {
    width: theme.spacing.unit * 4,
    textAlign: 'center',
  },
  bpmField: {
    height: 'auto',
  },
});

const Form = ({
  document: { name, isRhythmic, beats, bars, duration, bpm },
  onSave, classes, onChange, onClose, onRemove, canRemove,
}) => (
  <div>
    <DialogTitle>{name || 'Add a movement'}</DialogTitle>
    <DialogContent>

      <TextField
        fullWidth
        value={name}
        autoFocus
        onChange={onChange('name')}
        label="Name"
        margin="normal"
        required
      />

      <TextField
        fullWidth
        disabled={isRhythmic}
        value={`${isRhythmic ?
          (beats * bars * (60 / Number(bpm))).toFixed(1) :
          parseInt(duration / 1000, 10)
        }s`}
        label="Duration"
        margin="normal"
        InputProps={{
          startAdornment: <IconButton disabled={isRhythmic} onClick={onChange('duration', -1)}>
            <Icon>remove</Icon>
          </IconButton>,
          endAdornment: <IconButton disabled={isRhythmic} onClick={onChange('duration', 1)}>
            <Icon>add</Icon>
          </IconButton>,
          readOnly: true,
          classes: { input: classes.numberField },
        }}
      />

      <FormControlLabel
        control={<Checkbox checked={isRhythmic} onChange={onChange('isRhythmic')} />}
        label="Is rhythmic"
      />

      <Grid container spacing={16}>

        { isRhythmic && <Grid item xs sm={6} md={4}>
          <TextField
            fullWidth
            value={beats}
            label="Beats in a bar"
            margin="normal"
            InputProps={{
              startAdornment: <IconButton onClick={onChange('beats', -1)}><Icon>remove</Icon></IconButton>,
              endAdornment: <IconButton onClick={onChange('beats', 1)}><Icon>add</Icon></IconButton>,
              readOnly: true,
              classes: { input: classes.numberField },
            }}
          />
        </Grid> }

        { isRhythmic && <Grid item xs sm={6} md={4}>
          <TextField
            fullWidth
            value={bars}
            label="Number of bars"
            margin="normal"
            InputProps={{
              startAdornment: <IconButton onClick={onChange('bars', -1)}><Icon>remove</Icon></IconButton>,
              endAdornment: <IconButton onClick={onChange('bars', 1)}><Icon>add</Icon></IconButton>,
              readOnly: true,
              classes: { input: classes.numberField },
            }}
          />
        </Grid> }

        { isRhythmic && <Grid item xs md={4}>
          <TextField
            fullWidth
            value={bpm || ''}
            label="Beats per minute"
            margin="normal"
            type="number"
            onChange={onChange('bpm')}
            InputProps={{
              startAdornment: <Bpm beatsPerBar={beats} bpm={Number(bpm)} />,
              classes: { input: classes.bpmField },
            }}
          />
        </Grid> }

      </Grid>

    </DialogContent>
    <DialogActions>
      { canRemove && <IconButton onClick={onRemove}><Icon>delete</Icon></IconButton> }
      <Grid item xs />
      <Button onClick={onClose}>Cancel</Button>
      <Button color="primary" onClick={onSave} disabled={!name || name === 'Neutral'}>
        Save
      </Button>
    </DialogActions>
  </div>
);

Form.propTypes = {
  document: object,
  onSave: func,
  classes: object,
  onChange: func,
  onClose: func,
  onRemove: func,
  canRemove: bool,
};

export default compose(
  withStyles(styles),
  withHandlers({

    onChange: ({ document, onUpdate }) => (field, dir) => (ev) => {
      let newDocument = { ...document };
      switch(field) {
        case 'duration':
          newDocument.duration = Math.max(3000, document.duration + (dir * 1000));
          break;
        case 'bars':
          newDocument.bars = Math.max(1, document.bars + dir);
          break;
        case 'beats':
          newDocument.beats = Math.max(3, document.beats + dir);
          break;
        case 'bpm':
          newDocument.bpm = parseInt(Number(ev.target.value), 10);
          break;
        case 'isRhythmic':
          newDocument = { ...defaultValues };
          newDocument.isRhythmic = ev.target.checked;
          newDocument.key = document.key;
          newDocument.name = document.name;
          break;
        default:
          newDocument.name = ev.target.value;
          break;
      }
      onUpdate(newDocument);
    },

  }),

  setDisplayName('MovementForm'),
)(Form);
