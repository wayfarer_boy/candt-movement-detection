import React from 'react';
import { func, bool, object } from 'prop-types';
import { compose, withState, withHandlers, setDisplayName } from 'recompact';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';
import Grow from '@material-ui/core/Grow';

import { defaultValues } from '../helpers/movements.js';
import Form from './Form.jsx';

const Create = ({
  disabled, document, onUpdate, onSave, open, onClose, onOpen, onReset,
}) => (
  <Grid item xs>
    <Button
      fullWidth
      onClick={onOpen}
      color="secondary"
      disabled={disabled}
      style={{ borderRadius: 0, minWidth: 0 }}
      size="large"
    >
      Add
    </Button>
    <Dialog
      TransitionComponent={Grow}
      fullScreen
      open={open}
      onClose={onClose}
      onExited={onReset}
    >
      <Form
        document={document}
        onSave={onSave}
        onClose={onClose}
        onUpdate={onUpdate}
      />
    </Dialog>
  </Grid>
);

Create.propTypes = {
  onOpen: func,
  onClose: func,
  open: bool,
  onSave: func,
  onUpdate: func,
  document: object,
  disabled: bool,
  onReset: func,
};

export default compose(
  withState('open', 'setOpen', false),
  withState('document', 'setDocument', { ...defaultValues }),
  withHandlers({
    onOpen: ({ setOpen }) => () => setOpen(true),
    onClose: ({ setOpen }) => () => setOpen(false),
    onUpdate: ({ setDocument }) => newDoc => setDocument(newDoc),
    onSave: ({ document, onSave, setOpen }) => () => {
      onSave(document);
      setOpen(false);
    },
    onReset: ({ setDocument }) => () => {
      setDocument(defaultValues);
    },
  }),
  setDisplayName('MovementCreate'),
)(Create);
