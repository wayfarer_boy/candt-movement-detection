import React from 'react';
import { branch, setDisplayName, renderComponent } from 'recompact';
import { bool, func } from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';

import Create from './Create.jsx';

const Stop = ({ onStop }) => (
  <Grid container style={{ width: '100%' }}>
    <Grid item xs>
      <Button
        fullWidth
        onClick={onStop}
        style={{ zIndex: 1303, borderRadius: 0 }}
        size="large"
        variant="contained"
      >
        Stop
      </Button>
    </Grid>
  </Grid>
);

Stop.propTypes = {
  onStop: func,
};

Stop.displayName = 'Stop';

const Actions = ({ onHelp, isTraining, onSave, onTest, canTest, onTrain, isTesting, canTrain }) => (
  <Grid container alignItems="center" wrap="nowrap" style={{ width: '100%' }}>
    <Create onSave={onSave} disabled={isTraining} />
    <Grid item xs>
      <Button
        fullWidth
        disabled={isTraining || !canTrain || isTesting}
        color="primary"
        variant="contained"
        onClick={onTrain}
        style={{ minWidth: 0, borderRadius: 0 }}
        size="large"
      >
        Train
      </Button>
    </Grid>
    <Grid item xs>
      <Button
        fullWidth
        color="secondary"
        variant="contained"
        onClick={onTest}
        style={{ minWidth: 0, borderRadius: 0 }}
        size="large"
        disabled={isTraining || !canTest}
      >
        Test
      </Button>
    </Grid>
    <Grid item>
      <IconButton onClick={onHelp}><Icon>help</Icon></IconButton>
    </Grid>
  </Grid>
);

Actions.propTypes = {
  onSave: func,
  onTest: func,
  isTesting: bool,
  canTrain: bool,
  onTrain: func,
  canTest: bool,
  isTraining: bool,
  onHelp: func,
};

export default branch(
  ({ isTesting, isSampling }) => isTesting || isSampling,
  renderComponent(Stop),
  setDisplayName('Actions'),
)(Actions);
