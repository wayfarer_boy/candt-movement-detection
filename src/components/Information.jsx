import React from 'react';
import { number, func, bool } from 'prop-types';
import { compose, withProps, setDisplayName } from 'recompact';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepContent from '@material-ui/core/StepContent';
import StepLabel from '@material-ui/core/StepLabel';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Grow from '@material-ui/core/Grow';

const Information = ({ open, onClose, activeStep }) => (
  <Dialog TransitionComponent={Grow} fullScreen open={open}>
    <DialogContent>
      <Stepper activeStep={activeStep} orientation="vertical">
        <Step>
          <StepLabel>Add movements</StepLabel>
          <StepContent>
            Define a movement or set of movements you wish to teach to Prospero.
          </StepContent>
        </Step>
        <Step>
          <StepLabel>Add examples</StepLabel>
          <StepContent>
            Prospero learns using examples. Add as many examples of each movement as you can.
          </StepContent>
        </Step>
        <Step>
          <StepLabel>Train Prospero</StepLabel>
          <StepContent>
            Prospero can now be taught each of the movements using the examples you&apos;ve given. Adding more examples will help increase its intelligence.
          </StepContent>
        </Step>
        <Step>
          <StepLabel>Test Prospero</StepLabel>
          <StepContent>
            Prospero has been trained and is ready to recognise your movements. You can add more examples and retrain Prospero to make it more intelligent.
          </StepContent>
        </Step>
      </Stepper>
    </DialogContent>
    <DialogActions>
      <Button onClick={onClose}>Close</Button>
    </DialogActions>
  </Dialog>
);

Information.propTypes = {
  activeStep: number,
  onClose: func,
  open: bool,
};

export default compose(
  withProps(({ movements, trained }) => {
    let activeStep;
    if (movements.length === 0) {
      activeStep = 0;
    } else if (movements.some(({ thumbs }) => !thumbs || thumbs.length === 0)) {
      activeStep = 1;
    } else if (!trained) {
      activeStep = 2;
    } else {
      activeStep = 3;
    }
    return { activeStep };
  }),
  setDisplayName('MovementInformation'),
)(Information);
