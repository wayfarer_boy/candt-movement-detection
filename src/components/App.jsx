import React from 'react';
import { Provider } from 'react-redux';

import { store } from '../redux.js';
import List from './List.jsx';

const App = () => (
  <Provider store={store}>
    <List />
  </Provider>
);

export default App;
