import React from 'react';
import { bool, number, arrayOf, object } from 'prop-types';
import { compose, withProps, setDisplayName } from 'recompact';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';

import { imgWidth } from '../helpers/data.js';

const styles = theme => ({

  wrapper: {
    width: '100%',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    height: imgWidth,
    position: 'relative',
  },

  text: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },

  item: {
    backgroundColor: theme.palette.primary.main,
    boxSizing: 'border-box',
    paddingLeft: 1,
    paddingRight: 1,
    display: 'inline-block',
    transformOrigin: 'bottom center',
    transform: 'scaleY(0)',
    transition: 'transform .3s ease-in-out',
    height: '100%',
  },

});

const LossDisplay = ({ loaded, numSamples, items, classes, complete, enabled }) => (
  <div className={classes.wrapper}>
    { enabled && <Typography
      variant="headline"
      className={classes.text}
    >
      { loaded === 0 && loaded < numSamples && 'Preparing samples' }
      { !!numSamples && loaded < numSamples && `Sample ${loaded} of ${numSamples} loaded` }
      { !!numSamples && loaded === numSamples && complete === 0 && 'Preparing training data'}
      { !!complete && `Training ${complete} cycles` }
      <LinearProgress
        component="span"
        color={complete === 0 ? 'secondary' : 'primary'}
        variant="indeterminate"
      />
    </Typography> }
    { !enabled && items.map((style, index) => <div
      key={`epoch-${index}`}
      className={classes.item}
      style={style}
    />) }
  </div>
);

LossDisplay.propTypes = {
  items: arrayOf(object),
  classes: object,
  complete: number,
  enabled: bool,
  numSamples: number,
  loaded: number,
};

export default compose(
  withStyles(styles),
  withProps(({ loss = [] }) => {
    const max = Math.max(...loss, 0);
    const width = `${100 / loss.length}%`;
    const complete = loss.filter(l => l).length;
    return {
      complete,
      items: loss.map((l, i) => {
        const perc = l / max;
        return {
          transform: `scaleY(${complete ? perc : 0})`,
          width,
          translateDelay: `${i * 150}ms`,
        };
      }),
    };
  }),
  setDisplayName('MovementsLoss'),
)(LossDisplay);
