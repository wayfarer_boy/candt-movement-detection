import React from 'react';
import { withProps, lifecycle, setDisplayName, compose, withState, withHandlers } from 'recompact';
import { string, bool, object, number, arrayOf, func } from 'prop-types';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import Grid from '@material-ui/core/Grid';

import ListItem from './ListItem.jsx';
import withTest from '../containers/withTest.js';
import LossDisplay from './LossDisplay.jsx';
import withModel from '../containers/withModel.js';
import withMovements from '../containers/withMovements.js';
import withLiveData from '../containers/withLiveData.js';
import Countdown from './Countdown.jsx';
import { imgWidth } from '../helpers/data.js';
import Actions from './Actions.jsx';
import Information from './Information.jsx';

const styles = theme => ({
  wrapper: {
    height: '100%',
    width: '100%',
    overflow: 'hidden',
  },

  canvas: {
    width: imgWidth,
    height: imgWidth,
    position: 'fixed',
    bottom: 0,
    left: '50%',
    zIndex: theme.zIndex.modal,
    transform: 'translate(-50%, 200%) scale(1.5)',
    opacity: 0,
    transition: 'all .5s ease-in-out',
    pointerEvents: 'none',
    '&:after': {
      content: '',
      display: 'block',
      position: 'absolute',
      width: '100%',
      height: '100%',
      zIndex: theme.zIndex.modal + 1,
      backgroundImage: `linear-gradient(to bottom, white, transparent)`,
      top: 0,
      left: 0,
    },
  },

  canvasActive: {
    transform: `translateX(-50%) perspective(224px) rotateX(45deg) scale(1.5) translateY(-40px)`,
    opacity: 1,
  },

});

const List = ({
  sampling, onSave, movements, classes, onStop, onAddExample, onSetCanvas, onUpdate,
  canTrain, onTrain, isTraining, epochs, loss, onTest, isTesting, numSamples, loaded, trained,
  onSample, onRemove, help, onHelp, items, currentImage, setCurrentImageEl,
}) => (
  <Grid
    container
    direction="column"
    wrap="nowrap"
    alignItems="stretch"
    className={classes.wrapper}
  >
    <Grid item>
      { items.length === 0 && <ExpansionPanel>
        <ExpansionPanelSummary>
          <Typography className={classes.heading}>No movements to list</Typography>
        </ExpansionPanelSummary>
      </ExpansionPanel> }
    </Grid>
    { items.map(item => <Grid item key={item.key}>
      <ListItem
        movements={movements}
        item={item}
        onSample={onSample}
        onUpdate={onUpdate}
        onRemove={onRemove}
      />
    </Grid>) }
    <Grid item xs />
    <Slide direction="up" in={!!isTraining || !!loss.length}>
      <LossDisplay
        loaded={loaded}
        enabled={isTraining}
        loss={loss}
        total={epochs}
        numSamples={numSamples}
      />
    </Slide>
    <Actions
      onSave={onSave}
      onTest={onTest}
      onTrain={onTrain}
      isTesting={isTesting}
      canTrain={canTrain}
      canTest={trained}
      isSampling={!!sampling}
      onStop={onStop}
      isTraining={isTraining}
      onHelp={onHelp}
    />
    <canvas ref={onSetCanvas} className={`${classes.canvas} ${sampling ? classes.canvasActive : ''}`} />
    <img src={currentImage} ref={setCurrentImageEl} className={`${classes.canvas} ${isTesting ? classes.canvasActive : ''}`} />
    { !!sampling && <Countdown document={sampling} onSave={onAddExample} /> }
    <Information open={help} onClose={onHelp} movements={items} trained={trained} />
  </Grid>
);

List.propTypes = {
  movements: arrayOf(object),
  items: arrayOf(object),
  classes: object,
  sampling: object,
  onSave: func,
  onAdd: func,
  onAddExample: func,
  onSetCanvas: func,
  onStop: func,
  onUpdate: func,
  canTrain: bool,
  onTrain: func,
  isTraining: bool,
  loss: arrayOf(number),
  epochs: number,
  onTest: func,
  isTesting: bool,
  numSamples: number,
  loaded: number,
  trained: bool,
  onSample: func,
  onRemove: func,
  onHelp: func,
  help: bool,
  currentImage: string,
  setCurrentImageEl: func,
  currentImageEl: object,
};

export default compose(
  withStyles(styles),
  withState('sampling', 'setSampling'),
  withState('help', 'setHelp', false),
  withState('currentImageEl', 'setCurrentImageEl'),
  withMovements,
  withLiveData,
  withModel,
  withTest,

  withProps(({ movements }) => ({
    items: movements.filter(m => m.name !== 'Neutral'),
  })),

  withHandlers({

    onAddExample: ({ neutral, onAddSample, getCurrent, canvas, sampling }) => (isNeutral) => {
      if (isNeutral) {
        neutral.canvas.addExample(canvas);
        onAddSample(neutral, getCurrent());
      } else if (sampling) {
        sampling.canvas.addExample(canvas);
        onAddSample(sampling, getCurrent());
      }
    },

    onSample: ({ setSampling }) => (item) => () => setSampling(item),

    onStop: ({ onUpdate, setSampling, sampling, onTest, neutral }) => () => {
      if (sampling) {
        onUpdate(sampling);
        onUpdate(neutral);
        setSampling();
      } else {
        onTest();
      }
    },

    onHelp: ({ setHelp, help }) => () => setHelp(!help),

  }),

  lifecycle({
    componentDidMount() {
      console.log(this.props.movements);
      if (!this.props.movements.some(m => m.name === 'Neutral')) {
        this.props.createNeutral();
      }
    },
  }),

  setDisplayName('MovementList'),
)(List);
