/* global window */
import React from 'react';
import { object } from 'prop-types';
import { setDisplayName, compose, withState, withHandlers, lifecycle } from 'recompact';
import { withStyles } from '@material-ui/core/styles';

import withTimeout from '../containers/withTimeout.js';

let inactive = false;

const styles = theme => ({
  wrapper: {
    height: theme.spacing.unit * 6,
    width: theme.spacing.unit * 6,
    minWidth: theme.spacing.unit * 6,
    position: 'relative',
  },
  beat: {
    width: theme.spacing.unit * 2,
    height: theme.spacing.unit * 2,
    borderRadius: theme.spacing.unit,
    opacity: 0,
    transition: 'opacity 0s ease-in-out',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    minWidth: theme.spacing.unit * 2,
  },
});

const Bpm = ({ classes, style }) => (
  <div className={classes.wrapper}>
    <div className={classes.beat} style={style} />
  </div>
);

Bpm.propTypes = {
  classes: object,
  style: object,
};

export default compose(
  withTimeout,
  withStyles(styles, { withTheme: true }),
  withState('lastTime', 'setLastTime', 0),
  withState('beatNum', 'setBeatNum', 0),
  withState('style', 'setStyle'),

  withHandlers({
    onBeat: ({
      setLastTime, lastTime, theme, setStyle, beatsPerBar, setBeatNum, beatNum, bpm, style,
    }) => done => (time) => {
      if (!inactive) {
        const beat = 60000 / bpm;
        const nextTime = (lastTime || time) + beat;
        if (!lastTime || time >= nextTime) {
          setLastTime(nextTime);
          const backgroundColor = theme.palette[beatNum % beatsPerBar === 0 ? 'secondary' : 'primary'].main;
          setStyle({ backgroundColor, opacity: 1, transitionDuration: '0s' });
          setBeatNum(beatNum + 1);
        } else if (style.opacity) {
          setStyle({ ...style, opacity: 0, transitionDuration: `${beat}ms` });
        }
        window.requestAnimationFrame(done(done));
      } else {
        inactive = false;
      }
    },
  }),

  withHandlers({
    onInit: ({ onBeat }) => () => window.requestAnimationFrame(onBeat(onBeat)),
  }),

  lifecycle({
    componentDidMount() {
      this.props.onInit();
    },
    componentWillUnmount() {
      inactive = true;
    },
  }),

  setDisplayName('BpmVisualiser'),
)(Bpm);
