import React from 'react';
import { withHandlers, withState, compose, setDisplayName } from 'recompact';
import { bool, arrayOf, number, object, func } from 'prop-types';
import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import ButtonBase from '@material-ui/core/ButtonBase';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Badge from '@material-ui/core/Badge';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  details: {
    padding: 0,
  },

  heading: {
    fontSize: theme.typography.pxToRem(15),
  },

  secondary: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit,
  },

  thumbs: {
    overflowX: 'visible',
    overflowY: 'auto',
    maxWidth: '100%',
  },

  thumbsList: {
    whiteSpace: 'nowrap',
    width: 'auto',
    padding: '8px 24px 24px',
  },

  thumbWrapper: {
    display: 'inline-block',
    marginRight: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },

  thumb: {
    height: theme.spacing.unit * 6,
    width: theme.spacing.unit * 6,
    backgroundSize: '100% auto',
    backgroundRepeat: 'no-repeat',
  },

});

const ListItem = ({
  item, classes, selected, gyroDisabled, expanded, onChange, onToggleSelect,
  onRemoveExamples, onRemove, onSample, movements,
}) => (
  <ExpansionPanel expanded={item.key === expanded} onChange={onChange(item.key)}>
    <ExpansionPanelSummary expandIcon={<Icon>expand_more</Icon>}>
      <Typography noWrap className={classes.heading}>{item.name}</Typography>
      <Typography noWrap className={classes.secondary}>{item.secondary}</Typography>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails className={classes.details}>

      <div className={classes.thumbs}>
        <div className={classes.thumbsList}>
          { item.thumbs.map((style, i) => <ButtonBase
            onClick={onToggleSelect(i)}
            key={`${item.key}-image-${i}`}
            className={classes.thumbWrapper}
            centerRipple
          >
            <Badge
              color={selected.includes(i) ? 'primary' : 'default'}
              badgeContent={<Icon
                color="inherit"
                style={{
                  visibility: selected.includes(i) ? 'visible' : 'hidden',
                  fontSize: 'inherit',
                }}
              >
                check
              </Icon>}
            >
              <Paper
                elevation={selected.includes(i) ? 10:  2}
                className={classes.thumb}
                style={style}
              />
            </Badge>
          </ButtonBase>) }
        </div>
      </div>

    </ExpansionPanelDetails>
    <Divider />
    <ExpansionPanelActions>
      { !!selected.length && <Badge badgeContent={selected.length} color="error">
        <Button size="small" onClick={onRemoveExamples}>Remove</Button>
      </Badge> }
      { !selected.length && <Button size="small" onClick={onRemove(item)}>Remove</Button> }
      <Grid item xs />
      <Button size="small" disabled={gyroDisabled || movements.length < 2} color="primary" onClick={onSample(item)}>
        Add examples
      </Button>
    </ExpansionPanelActions>
  </ExpansionPanel>
);

ListItem.propTypes = {
  item: object,
  classes: object,
  onChange: func, 
  onToggleSelect: func, 
  onRemoveExamples: func, 
  onRemove: func, 
  onSample: func, 
  selected: arrayOf(number),
  gyroDisabled: bool,
  expanded: number,
  movements: arrayOf(object),
};

export default compose(
  withStyles(styles),
  withState('selected', 'setSelected', []),
  withState('expanded', 'setExpanded'),
  withHandlers({

    onChange: ({ setExpanded, expanded, setSelected }) => (id) => () => {
      if (expanded === id) {
        setExpanded();
      } else {
        setExpanded(id);
      }
      setSelected([]);
    },

    onRemoveExamples: ({ onUpdate, selected, setSelected, movements, expanded }) => () => {
      const movement = movements.find(({ key }) => key === expanded);
      if (movement) {
        [...selected]
          .sort((a, b) => b - a)
          .forEach(index => movement.canvas.removeExample(index));
        onUpdate(movement);
      }
      setSelected([]);
    },

    onToggleSelect: ({ setSelected, selected }) => index => () => {
      if (selected.includes(index)) {
        setSelected(selected.filter(i => i !== index));
      } else {
        setSelected([...selected, index]);
      }
    },

  }),
  setDisplayName('MovementListItem'),
)(ListItem);
