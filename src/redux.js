/* global window */
import { compose, createStore } from 'redux';
import persistState from 'redux-localstorage';
import reducers from './reducers.js';
import { initialState } from './helpers/movements.js';

const enhancer = compose(
  persistState(),
  typeof window !== 'undefined' && window.devToolsExtension ? window.devToolsExtension() : f => f,
);

export const store = createStore(reducers, { movements: initialState }, enhancer);
